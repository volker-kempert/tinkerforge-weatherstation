use std::{error::Error, io};
use std::thread;


use tinkerforge::{
    ip_connection::IpConnection,
    lcd_20x4_bricklet::*,
    };


const B_UID: &str = "fTK";
const LCD_UID: &str = "gCw";

enum HmiStates {
    BASE,
    TEMP_HIST,
    BAR_HIST,
    HYD_HIST,
    DEV_INFO,
}

struct {
    uid: str,
    state: HmiStates,
}


fn main() -> Result<(), Box<dyn Error>> {
    let ipcon = IpConnection::new(); // Create IP connection.
    let lcd = Lcd20x4Bricklet::new(LCD_UID, &ipcon); // Create device object.
    ipcon.connect((HOST, PORT)).recv()??; // Connect to brickd.
                                          // Don't use device before ipcon is connected.

    // Turn backlight on
    lcd.backlight_on();

    // Write "Hello World"
    lcd.write_line(0, 0, prn_illuminance.to_string());
    lcd.write_line(1, 0, prn_humidity.to_string());
    lcd.write_line(3, 0, prn_temperature.to_string());

    let button_pressed_receiver = lcd.get_button_pressed_callback_receiver();

    // Spawn thread to handle received callback messages.
    // This thread ends when the `lcd` object
    // is dropped, so there is no need for manual cleanup.
    thread::spawn(move || {
        for button_pressed in button_pressed_receiver {
            println!("Button Pressed: {}", button_pressed);
        }
    });

    let button_released_receiver = lcd.get_button_released_callback_receiver();

    // Spawn thread to handle received callback messages.
    // This thread ends when the `lcd` object
    // is dropped, so there is no need for manual cleanup.
    thread::spawn(move || {
        for button_released in button_released_receiver {
            println!("Button Released: {}", button_released);
        }
    });

    println!("Press enter to exit.");
    let mut _input = String::new();
    io::stdin().read_line(&mut _input)?;
    ipcon.disconnect();
    Ok(())
}