use std::error::Error;
use std::{thread, time};

// Tinkerforge documentation
// see:
// https://www.tinkerforge.com/de/doc/Software/API_Bindings_Rust.html
//
use tinkerforge::{
    barometer_bricklet::*,
    ip_connection::IpConnection,
    lcd_20x4_bricklet::*,
    ambient_light_bricklet::*,
    humidity_v2_bricklet::*,
    };

const HOST: &str = "localhost";
const PORT: u16 = 4223;

const B_UID: &str = "fTK";
const LCD_UID: &str = "gCw";
const A_UID: &str = "gvW";
const H_UID: &str = "fRk";

fn main() -> Result<(), Box<dyn Error>> {
    let ipcon = IpConnection::new(); // Create IP connection.
    let b = BarometerBricklet::new(B_UID, &ipcon); // Create device object.
    let lcd = Lcd20x4Bricklet::new(LCD_UID, &ipcon); // Create device object.
    let al = AmbientLightBricklet::new(A_UID, &ipcon); // Create device object.
    let h = HumidityV2Bricklet::new(H_UID, &ipcon); // Create device object.

    ipcon.connect((HOST, PORT)).recv()??; // Connect to brickd.
                                          // Don't use device before ipcon is connected.

    loop {
        // Get current air pressure.
        let air_pressure = b.get_air_pressure().recv()?;
        let prn_air_pressure = format!("Druck: {} mbar", air_pressure as f32 / 1000.0);
        println!("{}", prn_air_pressure);

        let temperature = b.get_chip_temperature() .recv()?;
        let prn_temperature = format!("Temperatur: {} °C", temperature as f32 / 100.0 );
        println!("{}", prn_temperature);

        // Get current Illuminance.
        let illuminance = al.get_illuminance().recv()?;
        let prn_illuminance = format!("Licht: {} lx", illuminance as f32 / 100.0);
        println!("{}", prn_illuminance);

        // Get current humidity.
        let humidity = h.get_humidity().recv()?;
        let prn_humidity = format!("Feuchte: {} %RH", humidity as f32 / 100.0);
        println!("{}", prn_humidity);

        // Turn backlight on
        lcd.backlight_on();

        lcd.write_line(0, 0, prn_illuminance.to_string());
        lcd.write_line(1, 0, prn_humidity.to_string());
        lcd.write_line(2, 0, prn_air_pressure.to_string());
        lcd.write_line(3, 0, prn_temperature.to_string());

        // wait a second
        let sleep = time::Duration::from_millis(1000);
        // let now = time::Instant::now();
        thread::sleep(sleep);
    }

    // ipcon.disconnect();
}