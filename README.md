# Rust Weather Station

## Objective

Program the weather station from Tinkerforge via Rust programming language.

Envisioned Learnings:

* Integrate Rust into Yocto
* Integrate Rust & Tinkerforge

Envisioned Features:

* Show Date/Time, temperature, etc on display
* Record data measurements
* Show some statistics, react on buttons

## TODO

* include config crate
* multiple reads record history (rolling in memory)
* investigate callback of sensors
* show history by buttons
* publish as webapi ()

## References

https://www.tinkerforge.com/de/doc/Kits/WeatherStation/WeatherStation.html
https://www.tinkerforge.com/de/doc/Software/API_Bindings_Rust.html
https://github.com/Tinkerforge/weather-station

Brickviewer

https://www.tinkerforge.com/de/doc/Software/Brickv.html
